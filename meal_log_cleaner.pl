#!/usr/bin/perl

use strict;
use warnings;

my $date = `date`;
my $file_line_limit = 9000;
my $file_lines = `cat /home/pi/meals.log | wc -l`;
chomp($file_lines);

if( $file_lines > $file_line_limit ) {  
  system("echo \"Backed up on $date\" > /home/pi/meals.log");	
  system("mv /home/pi/meals.log /home/pi/meals.bak");
}
system("echo \"Still active: \`date\`\" > /home/pi/cleanerStatus.tmp");
