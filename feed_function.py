#!/usr/bin/env python

import time
import datetime
import smtplib
from datetime import date, timedelta, datetime
from pytz import timezone

# Import the Raspberry Pi GPIO library.
import RPi.GPIO as GPIO

try:
   Servo1Pin=18
   Servo2Pin=23

   # Beeper to announce when food is about to drop
   BeeperPin=24

   # Tell the GPIO who is what,EG: OUTPUTS , beeper, 
   GPIO.setmode(GPIO.BCM)
   GPIO.setup(BeeperPin, GPIO.OUT)

   # Set the beeper pins low
   GPIO.output(BeeperPin, False)

   # Setup the Servos Pins
   GPIO.setup(Servo1Pin, GPIO.OUT)
   GPIO.setup(Servo2Pin, GPIO.OUT)

   def dual_servo_CCW(Servo1PIN,Servo2PIN):
     # Set servo on Servo1Pin to 2000us (2.0ms)
     servo1 = GPIO.PWM(Servo1PIN, 60)
     servo2 = GPIO.PWM(Servo2PIN, 60)

     sleepTime = 0.45
     
     #servo1.start(9) # RIP Bravery. We will miss you always. #
     #servo2.start(10) # RIP Chuck. We will miss you always. #
     time.sleep(sleepTime)
     servo1.stop()
     servo2.stop()

   def feed_cats():
      GPIO.output(BeeperPin, True)
      time.sleep(.25)
      GPIO.output(BeeperPin, False)
      time.sleep(.25)
      GPIO.output(BeeperPin, True)
      time.sleep(.25)
      GPIO.output(BeeperPin, False)
      time.sleep(.25)
      GPIO.output(BeeperPin, True)
      time.sleep(.25)
      GPIO.output(BeeperPin, False)
      print "Ok, food is coming out at both ends!"
      #servo_CCW(Servo2Pin,FeedTime)
      dual_servo_CCW(Servo1Pin,Servo2Pin)
   
   def log_to_file(FeedTime):
	file = open('./meals.log', 'a')
	string = 'The cats were fed at {0}\n'.format(FeedTime)
	file.write(string)

   # Setup the Email stuff.
   def send_email(hopper,feedtime):
    feedtime = feedtime.strftime("%a %b %d at %I:%M%p")
    d2 = datetime.today() + timedelta(days=1)
    to = 'genericemailaddress867@gmail.com'
    gmail_user = 'cat.feeder.bot@gmail.com'
    gmail_pwd = 'iamabotthatfeedscats'
    smtpserver = smtplib.SMTP("smtp.gmail.com",587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(gmail_user, gmail_pwd)
    header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject: CatFeeder: I fed the cats on ' + feedtime + '\n'
    msg = header + '\nI fed the cats using ' + hopper + ' hopper(s)\n\nThey were fed on ' + feedtime + '.\n\n'
    smtpserver.sendmail(gmail_user, to, msg)
    print 'email sent!'
    smtpserver.close()

   # Main #

   d = datetime.now()
   year = d.year
   month = d.month
   day = d.day

   feedTime1 = datetime(year, month, day, 6, 0)
   print "Feed Time 1: '%s'" % (feedTime1)
   feedTime2 = datetime(year, month, day, 14, 30)
   print "Feed Time 2: '%s'" % feedTime2
   feedTime3 = datetime(year, month, day, 22, 0)
   print "Feed Time 3: '%s' " % feedTime3

   feedtime=datetime.now(timezone('US/Eastern'))
   print "Cat Feeding time %s" % feedtime.strftime("%Y-%m-%d %H:%M") ##:%S")
   feed_cats()
   log_to_file(feedtime)
   send_email("both",feedtime)
   print "Cats were fed successfully"

except KeyboardInterrupt:
	GPIO.cleanup()	

finally:
	GPIO.cleanup()
